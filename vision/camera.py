# -*- coding: utf-8 -*-

from scipy.linalg import expm, rq, det, inv
import matplotlib.pyplot as plt

from math import atan
from vision.rt_matrix import rotation_matrix
import autograd.numpy as np


class Camera(object):
    """ Class for representing pin-hole cameras. """
    def __init__(self):
        """ Initialize P = K[R|t] camera model. """
        self.P = np.mat(np.eye(3,4))
        self.K = np.mat(np.eye(3, dtype=np.float64))  # calibration matrix
        self.R = np.mat(np.eye(3, dtype=np.float64)) # rotation
        """t is the position of the origin of the world coordinate system expressed in coordinates of the camera-centered coordinate system"""        
        self.t = np.mat(np.zeros(3)).T # translation 
        self.Rt = np.mat(np.eye(4, dtype=np.float64)) # translation and displacement in homgeneous coordinates
        self.fx = 1.
        self.fy = 1.
        self.cx = 0.
        self.cy = 0.
        self.img_width = 1280
        self.img_height = 960

    def clone_withPose(self, tvec, rmat):
        new_cam = Camera()
        new_cam.K = self.K
        new_cam.set_R_mat(rmat)
        new_cam.set_t(tvec[0], tvec[1],  tvec[2])
        new_cam.set_P()
        new_cam.img_height = self.img_height
        new_cam.img_width = self.img_width
        return new_cam

    def clone(self):
        new_cam = Camera()
        new_cam.P = self.P
        new_cam.K = self.K
        new_cam.R = self.R
        new_cam.t = self.t
        new_cam.Rt = self.Rt
        new_cam.fx = self.fx
        new_cam.fy = self.fy
        new_cam.cx = self.cx
        new_cam.cy = self.cy
        new_cam.img_height = self.img_height
        new_cam.img_width = self.img_width
        return new_cam


    def set_P(self):
        # P = K[R|t]
        # P is a 3x4 Projection Matrix (from 3d euclidean to image)
        #self.Rt = hstack((self.R, self.t))
        self.P = np.dot(self.K, self.Rt[:3,:4])

    def set_K(self, fx = 1, fy = 1, cx = 0,cy = 0):
        # K is the 3x3 Camera matrix
        # fx, fy are focal lenghts expressed in pixel units
        # cx, cy is a principal point usually at image center
        self.fx = fx
        self.fy = fy
        self.cx = cx
        self.cy = cy
        self.K = np.mat([[fx, 0, cx],
                         [0,fy,cy],
                         [0,0,1.]], dtype=np.float64)
        self.set_P()
        
    def set_K_mat(self, K):
        self.K = K
        self.set_P()
        

    def set_width_heigth(self,width, heigth):
        self.img_width = width
        self.img_height = heigth

    def update_Rt(self):
        #self.Rt = np.dot(self.t,self.R)
        
        self.Rt = np.mat(np.eye(4, dtype=np.float64))
        self.Rt[:3,:3] = self.R
        self.Rt[:3,3] = self.t
        
        #self.Rt  = self.R
        #self.Rt[:3,3] = self.t[:3,3]
        self.set_P()

    def set_R_axisAngle(self,x,y,z, alpha):
        """  Creates a 3D [R|t] matrix for rotation
        around the axis of the vector defined by (x,y,z)
        and an alpha angle."""
        #Normalize the rotation axis a
        a = np.array([x,y,z])
        a = a / np.linalg.norm(a)

        #Build the skew symetric
        a_skew = np.mat([[0,-a[2],a[1]], [a[2], 0, -a[0]], [-a[1], a[0], 0]])
        R = np.mat(np.eye(3, dtype=np.float32))
        R[:3,:3] = expm(a_skew*alpha)
        self.R = R
        self.update_Rt()

    def set_R_mat(self,R):
        self.R = np.mat(R[:3,:3])
        self.update_Rt()


    """t is the position of the origin of the world coordinate system expressed in coordinates of the camera-centered coordinate system"""
    def set_t(self, x,y,z, frame = 'camera'):
        #self.t = array([[x],[y],[z]])
        self.t = np.mat(np.zeros(3)).T 
        """ Using frame world forces to set R to identity """
        if frame=='world':
          cam_in_world = np.mat(np.array([x,y,z])).T
          cam_t = self.R*(-cam_in_world)
          self.t = cam_t
        else:
          self.t = np.mat(np.array([x,y,z])).T
        self.update_Rt()

    def get_normalized_pixel_coordinates(self, X):
        """
        These are in normalised pixel coordinates. That is, the effects of the
        camera's intrinsic matrix and lens distortion are corrected, so that
        the Q projects with a perfect pinhole model.
        """
        return np.dot(inv(self.K), X)

    def addnoise_imagePoints(self, imagePoints, mean = 0, sd = 2):
        """ Add Gaussian noise to image points
        imagePoints: 3xn points in homogeneous pixel coordinates
        mean: zero mean
        sd: pixels of standard deviation
        """
        imagePoints = np.copy(imagePoints)
        if sd > 0:
            gaussian_noise = np.random.normal(mean,sd,(2,imagePoints.shape[1]))
            imagePoints[:2,:] = imagePoints[:2,:] + gaussian_noise
        return imagePoints

    """t is the position of the origin of the world coordinate system expressed in coordinates of the camera-centered coordinate system"""
    def get_tvec(self):
        tvec = self.t[0:3]
        return tvec

    """t_in_world is the position of the camera in coordinates of the world-centered coordinate system"""
    def get_world_position(self):
        t_in_world = np.dot(inv(self.Rt), np.array([0,0,0,1]))
        return t_in_world


    def project(self,X, quant_error=False):
        """  Project points in X (4*n array) and normalize coordinates. """
        self.set_P()
        x = np.dot(self.P,X)
        x = x/x[2]
        if(quant_error):
          x = np.around(x, decimals=0)
        return x

    def project_circle(self, circle):
        C = circle.get_C
        H = self.homography_from_Rt()
        Q = None

    def plot_image(self, imgpoints, points_color = 'blue'):
        # show Image
        # plot projection
        plt.figure("Camera Projection")
        plt.plot(imgpoints[0],imgpoints[1],'.',color = points_color)
        #we add a key point to help us see orientation of the points
        #plt.plot(imgpoints[0,0],imgpoints[1,0],'.',color = 'blue')
        plt.xlim(0,self.img_width)
        plt.ylim(0,self.img_height)
        plt.gca().invert_yaxis()
        plt.gcf().gca().set_aspect('equal')
        plt.show()

    def plot_plane(self, plane, axes):
        if plane.type == 'rectangular':
            corners = plane.get_corners()
            img_corners = np.array(self.project(corners))
            img_corners =np.c_[img_corners,img_corners[:,0]]
            plt.plot(img_corners[0],img_corners[1])
        elif plane.type == 'circular':
            c = plane.circle
            c_projected = c.project(self.homography_from_Rt())
            c_projected.contour(axes)
            

    def factor(self):
        """  Factorize the camera matrix into K,R,t as P = K[R|t]. """
        # factor first 3*3 part
        K,R = rq(self.P[:,:3])
        # make diagonal of K positive
        T = np.diag(np.sign(np.diag(K)))
        if det(T) < 0:
            T[1,1] *= -1
        self.K = np.dot(K,T)
        self.R = np.dot(T,R) # T is its own inverse
        self.t = np.dot(inv(self.K),self.P[:,3])
        return self.K, self.R, self.t

    def fov(self):
        """ Calculate field of view angles (grads) from camera matrix """
        fovx = np.rad2deg(2 * atan(self.img_width / (2. * self.fx)))
        fovy = np.rad2deg(2 * atan(self.img_height / (2. * self.fy)))
        return fovx, fovy

    def move(self, x,y,z):
        Rt = np.identity(4);
        Rt[:3,3] = np.array([x,y,z])
        self.P = np.dot(self.K, self.Rt)
        
    def rotate_camera(self, axis, angle):
        """ rotate camera around a given axis in CAMERA coordinate, please use following Rt"""
        R = rotation_matrix(axis, angle)
        newR = np.dot(R,self.R)
        self.Rt = np.dot(self.t, newR)
        self.R[:3,:3] = self.Rt[:3,:3]
        self.t[:3,3] = self.Rt[:3,3]
        self.set_P()

    def rotate(self, axis, angle):
        """ rotate camera around a given axis in world coordinates"""
        R = rotation_matrix(axis, angle)
        self.Rt = np.dot(R, self.Rt)
        self.R = self.Rt[:3,:3]
        self.t = self.Rt[:3,3]
        # DO NOT forget to set new P
        self.set_P()

    def rotate_x(self,angle):
        self.rotate(np.array([1,0,0],dtype=np.float32), angle)

    def rotate_y(self,angle):
        self.rotate(np.array([0,1,0],dtype=np.float32), angle)

    def rotate_z(self,angle):
        self.rotate(np.array([0,0,1],dtype=np.float32), angle)

    def look_at(self, x,y,z):
      # Get target in wordl coordinates
      target_in_world = np.mat(np.array([x,y,z])).T    
      eye = np.mat(self.get_world_position()[:3]).T  # cam in cam coordinates

      #target = np.array([0,0,0])
      up = np.mat(np.array([0,1,0])).T

      zaxis = np.nan_to_num((target_in_world-eye)/np.linalg.norm(target_in_world-eye))
      
      # up cannot be in the same direction of zaxis, if this happens cross product wont work
      if(up.all() == zaxis.all()):
          up = np.mat(np.array([1,0,0])).T
      
      xaxis = np.nan_to_num((np.cross(up.T,zaxis.T)).T/np.linalg.norm(np.cross(up.T,zaxis.T)))
      yaxis = np.nan_to_num(np.cross(zaxis.T, xaxis.T).T)
      
#      R = np.mat(np.array([[xaxis[0,0], yaxis[0,0], zaxis[0,0]],
#                           [xaxis[1,0], yaxis[1,0], zaxis[1,0]],
#                           [xaxis[2,0], yaxis[2,0], zaxis[1,0]]]))

      R = np.array([[xaxis[0,0], xaxis[1,0], xaxis[2,0]],
                   [yaxis[0,0], yaxis[1,0], yaxis[2,0]],
                   [zaxis[0,0], zaxis[1,0], zaxis[2,0]]])

      new_t = R*self.t
      self.t = new_t
      self.R = R
      self.update_Rt()

    def homography_from_Rt(self):
      rt_reduced = self.Rt[:3,[0,1,3]]
      H = np.dot(self.K,rt_reduced)
      if H[2,2] != 0.:
        H = H/H[2,2]
      return H

if __name__ == "__main__":
    """ ---- Test LOOK AT ------"""
    print("Test look_at function")
    from vision.plane import Plane
    from vision.plot_tools import plot3D
    origin = Camera()
    origin.update_Rt()
    cam1 = Camera()
    cam1.set_t(1, 1, 1, frame = 'world')  # R defaults to identity     
    cam1.look_at(0,0,0)
    
    cam2 = Camera()
    cam2.set_t(1, 1, 1, frame = 'camera')  # R defaults to identity     
    cam2.look_at(0,0,0)
        
    plane_size = (0.3,0.3)
    plane =  Plane(origin=np.array([0, 0, 0] ), normal = np.array([0, 0, 1]), size=plane_size, n = (2,2))
    
    plane.set_origin(np.array([0, 0, 0]))
    plane.uniform()
    planes = []
    planes.append(plane)
    plot3D([origin, cam1, cam2], planes, axis_scale = 1.0)
    
    """Test that rotate function doesnt change camera position in world matrix"""
    print("Test rotate functions")
    from vision.rt_matrix import rot_matrix_error
    cam3 = Camera()    
    cam3.set_t(0,0,-2.5, frame = 'camera')
    R1= cam3.R
    t1 = cam3.t
    Rt1 = cam3.Rt
    pos1 = cam3.get_world_position()
    cam3.set_P()
    plot3D([cam3], planes, axis_scale = 1.0)
    
    cam3.rotate_y(np.deg2rad(+170.))
    cam3.rotate_x(np.deg2rad(+0.))
    cam3.rotate_z(np.deg2rad(+0.))
    cam3.set_P()
    plot3D([cam3], planes, axis_scale = 1.0)
    R2 = cam3.R
    t2 = cam3.t
    Rt2 = cam3.Rt
    pos2 = cam3.get_world_position()
    print (pos1-pos2)
    
    plot3D([cam3], planes, axis_scale = 1.0)
    print("Angle: ", rot_matrix_error(R1, R2))
    
    """ Test that projection matrix doesnt change rotation and translation"""
    print("Test projection matrix")
    cam4 = Camera()   
    cam4.set_t(0,0,-2.5, frame = 'camera')
    R1 = cam4.R
    t1 = cam4.t
    Rt1 = cam4.Rt
    pos1 = cam4.get_world_position()
    cam4.set_P()
    R2 = cam4.R
    t2 = cam4.t
    Rt2 = cam4.Rt
    pos2 = cam4.get_world_position()
    
    print (pos1-pos2)
    print (R1 - R2)
    print (t1 - t2)
    print (Rt1 - Rt2)